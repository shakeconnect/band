#include <RFduinoBLE.h>

float app_id = 1234;

void setup() 
{
        pinMode(6, OUTPUT);
        pinMode(5, INPUT);
        digitalWrite(6, HIGH);
        RFduinoBLE.advertisementData = "button";
        RFduinoBLE.begin();
}

void loop() 
{
        if (digitalRead(5) == LOW)
        {
                RFduinoBLE.sendFloat(app_id);
                RFduino_ULPDelay( SECONDS(0.2) );
        }
        
}
