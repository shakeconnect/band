#include <fft.h>

const Complex test[] = { 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0 };
CArray data(test, 8);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  fft(data);
  for (int i=0; i<8; i++)
  {
    Serial.print(data[i].real());
    Serial.print(" ");
    Serial.println(data[i].imag());
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
