#include "I2Cdev.h"
#include <fft.h>
#include <RFduinoBLE.h>
#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"

MPU6050 mpu;
#define OUTPUT_READABLE_WORLDACCEL

#define LED_PIN 4 // (Arduino is 13, Teensy is 11, Teensy++ is 6)

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor umeasurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// Acceleration/time measurements
float time_meas;

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
int dmpDataReady(uint32_t ulPin) 
{
        mpuInterrupt = true;
}

void setup() {
        // join I2C bus (I2Cdev library doesn't do this automatically)
        Wire.begin();
        
        // initialize serial communication
        // (115200 chosen because it is required for Teapot Demo output, but it's
        // really up to you depending on your project)
        Serial.begin(9600);
        
        mpu.initialize();
        
        devStatus = mpu.dmpInitialize();
        
        // supply your own gyro offsets here, scaled for min sensitivity
        mpu.setXGyroOffset(220);
        mpu.setYGyroOffset(76);
        mpu.setZGyroOffset(-85);
        mpu.setZAccelOffset(1788); // 1688 factory default for my test chip
        
        // make sure it worked (returns 0 if so)
        if (devStatus == 0) 
        {
                // turn on the DMP, now that it's ready
                mpu.setDMPEnabled(true);
                
                // enable Arduino interrupt detection
                pinMode(2,INPUT);
                mpuIntStatus = mpu.getIntStatus();
                RFduino_pinWakeCallback(2,HIGH,dmpDataReady);
                mpuIntStatus = mpu.getIntStatus();
                
                // set our DMP Ready flag so the main loop() function knows it's okay to use it
                dmpReady = true;
                
                // get expected DMP packet size for later comparison
                packetSize = mpu.dmpGetFIFOPacketSize();
        } 
        else 
        {
        }
        
        // configure LED for output
        pinMode(LED_PIN, OUTPUT);
}

void loop() 
{
        // if programming failed, don't try to do anything
        if (!dmpReady) return;
        
        // wait for MPU interrupt or extra packet(s) available
        while (!mpuInterrupt && fifoCount < packetSize) 
        {
              if (aaWorld.y > 3000 || aaWorld.y < -3000)
                digitalWrite(LED_PIN, HIGH);
              else
                digitalWrite(LED_PIN, LOW);
                
        }
        
        // reset interrupt flag and get INT_STATUS byte
        mpuInterrupt = false;
        mpuIntStatus = mpu.getIntStatus();
        
        // get current FIFO count
        fifoCount = mpu.getFIFOCount();
        
        // check for overflow (this should never happen unless our code is too inefficient)
        if ((mpuIntStatus & 0x10) || fifoCount == 1024) 
        {
                // reset so we can continue cleanly
                mpu.resetFIFO();
                
                // otherwise, check for DMP data ready interrupt (this should happen frequently)
        } 
        else if (mpuIntStatus & 0x02) 
        {
                // wait for correct available data length, should be a VERY short wait
                while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
                
                // read a packet from FIFO
                mpu.getFIFOBytes(fifoBuffer, packetSize);
                
                // track FIFO count here in case there is > 1 packet available
                // (this lets us immediately read more without waiting for an interrupt)
                fifoCount -= packetSize;

                // display initial world-frame acceleration, adjusted to remove gravity
                // and rotated based on known orientation from quaternion
                mpu.dmpGetQuaternion(&q, fifoBuffer);
                mpu.dmpGetAccel(&aa, fifoBuffer);
                mpu.dmpGetGravity(&gravity, &q);
                mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
                mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);
                Serial.println(aaWorld.y);
        }
}
